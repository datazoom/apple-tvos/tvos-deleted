//
//  DZCollectorTvOSBase.swift
//  DZCollectorTvOSBase
//
//  Created by Vuk Simovic on 2.3.22..
//  Copyright © 2021 RedCellApps. All rights reserved..
//

import Foundation
import AVFoundation
import MediaPlayer
import AVKit
import Network
#if !os(macOS)
import UIKit
import TVUIKit
#endif
import AdSupport

open class DZCollectorTvOSBase : DZConnector {
    public static let appSessionTimeout = 120.0
    
    public var eventList: [String] = []
    
    public var eventTypesVersion = ""
    public var eventContentList: [String] = []
    public var eventAdList: [String] = []
    
    public var dzInit: Int = -1
    
    public var milestoneTimer : Timer?
    public var networkMonitor = NWPathMonitor()
    public var connectionType = ""
    public var cdn = ""
    public var contentRequestingCount = 0
    public var numberOfVideosPlayed = 0
    
    public var isPlaying: Bool = false
    public var isPlaybackComplete: Bool = false
    public var isComplete: Bool = false
    public var isPlay: Bool = false
    public var isBuffering: Bool = false
    public var isPlaybackStart: Bool = false
    public var loopIndex:Int = 1
    
    public var playerMetadata: [String : Any]? = nil
    public var sessionMetadata: [String : Any]? = nil
    public var customMetadata: [String : Any]? = nil

    public var engagementStart: Double = 0.0
    
    var mediaRequestTime: Double = 0.0
    
    public var playerBitRate: Double? = 0.0
    public var renditionHeight: Int? = 0
    public var renditionWidth: Int? = 0
    public var renditionVideoBitrate: Double? = 0
    public var absShift: String = ""
    
    public var originalVideoURLString: String?
    public var adTagURLString: String?
    public var adId: String?
    public var adSourceUrl: String?
    public var adTitle: String?
    public var adContentType : String?
    public var adSystem: String?
    public var adWrapper: [String] = []
    public var adPosition: Int?
    
    
    
    
    
    func resetCounters() {
        self.numberOfVideosPlayed = 0
        self.contentRequestingCount = 0
    }
    
    //MARK:- Create Connector
    @objc open func createBaseCollector(configID:String, url:String, onCompletion: @escaping (Bool,Error?) -> Void) {
        if networkMonitor.currentPath.usesInterfaceType(.wifi) {
            connectionType = "WiFi"
        }
        else {
            connectionType = "Lan"
        }

        connectorDelegate = self
        
        if self.dzInit == -1 {
            self.dzInit = 0
            initialise(withConfigId: configID, andURL: url) { (success, error) in
                print("%%%% Create and initialize base collector - done %%%% \n")
                if success == true {
                    self.eventList = DZWebBroker.shared.eventList
                    
                    self.eventTypesVersion = DZWebBroker.shared.eventTypesVersion
                    self.eventContentList = DZWebBroker.shared.eventContentList
                    self.eventAdList = DZWebBroker.shared.eventAdList
                    
                    self.resetCounters()
                    DZEventCollector.shared.resetCounters()
                    
                    self.engagementStart = CACurrentMediaTime()*1000
                    DZEventCollector.shared.engagementStart = self.engagementStart
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self.dzInit = 1
                        onCompletion(true, nil)
                    }
                } else {
                    onCompletion(false, error)
                }
            }
        }
        else if self.dzInit == 0 {
            print("%%%% Initialization in progress %%%% \n")
        } else {
            print("%%%% Initialization already done %%%% \n")
        }
    }
}

extension DZCollectorTvOSBase {
    public func initCustomMetadata(_ playerMetadata:[String:Any]?, _ sessionMetadata:[String:Any]?, _ customMetadata:[String:Any]?) {
        self.playerMetadata = playerMetadata
        self.sessionMetadata = sessionMetadata
        self.customMetadata = customMetadata
    }
}

extension DZCollectorTvOSBase: DZConnectorProtocol {
    public func onCreateAppSessionID() {
        UserDefaults.standard.clearUserContentSessionID()
        UserDefaults.standard.clearUserAdSessionID()
        UserDefaults.standard.clearUserSessionViewID()
        
        UserDefaults.standard.clearUserRequestID()
        
        onResetAppSessionID()
    }
    
    public func onResetAppSessionID() {
        DZEventCollector.shared.engagementStart = CACurrentMediaTime()*1000
        DZEventCollector.shared.details.metrics.engagementDuration = 0
        
        DZEventCollector.shared.details.metrics.numberErrorsContent = 0
        DZEventCollector.shared.details.metrics.numberErrorsAds = 0
        DZEventCollector.shared.details.metrics.numberErrors = 0
    }
    
    public func onCreateContentSessionID() {
        
    }
    
    public func onResetContentSessionID() {
        
    }
}


