//
//  DataCollector.swift
//  DZCollectorTvOSBase
//
//  Created by Vuk on 6.11.21..
//  Copyright © 2021 RedCellApps. All rights reserved.
//

import Foundation

//MARK:- Data_collector
public struct DZConfigDataCollector {
    public let url : String?
    public let port : Int?
    
}

extension DZConfigDataCollector : Decodable {
    enum CodingKeys: String, CodingKey {
        case url = "url"
        case port = "port"
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        url = try values.decodeIfPresent(String.self, forKey: .url)
        port = try values.decodeIfPresent(Int.self, forKey: .port)
    }
    
}
