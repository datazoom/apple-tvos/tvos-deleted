//
//  DZEvent.swift
//  DZCollectorTvOSBase
//
//  Created by Vuk on 6.11.21..
//  Copyright © 2021 RedCellApps. All rights reserved.
//

import Foundation

//MARK:- Events
public struct DZConfigEvents {
    public let types : [DZConfigTypeElement]?
    
    public let metdata : [String]?
    public let flux_data : [String]?
    public let interval : Int?
}

extension DZConfigEvents : Decodable {
    
    enum CodingKeys: String, CodingKey {
        case types = "types"
        case metdata = "metdata"
        case flux_data = "flux_data"
        case interval = "interval"
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        types = try values.decodeIfPresent([DZConfigTypeElement].self, forKey: .types)
        metdata = try values.decodeIfPresent([String].self, forKey: .metdata)
        flux_data = try values.decodeIfPresent([String].self, forKey: .flux_data)
        interval = try values.decodeIfPresent(Int.self, forKey: .interval)
    }
}

