//
//  DZConfigEventTypes.swift
//  DZCollectorTvOSBase
//
//  Created by Vuk on 1.2.22..
//  Copyright © 2022 Adhoc Technologies. All rights reserved.
//

import Foundation

public struct DZConfigEventTypes {
    public var typesContent : [DZConfigTypeElement]? = []
    public var typesAd : [DZConfigTypeElement]? = []
    
    private static func getTypesForTypeData(_ types: [DZConfigTypeElementV3], _ type: String) -> [DZConfigTypeElement] {
        var retVal: [DZConfigTypeElement] = []
        let naItems = types.filter{ $0.mediaTypes == nil || $0.mediaTypes?.count == 0 || $0.mediaTypes?.contains("not_appl") == true || $0.mediaTypes?.contains("na") == true }.map { $0.name }.compactMap { $0 }
        let items = types.filter{ $0.mediaTypes?.contains(type) == true }.map { $0.name }.compactMap { $0 }
        
        naItems.forEach { item in
            retVal.append(DZConfigTypeElement(item))
        }
        
        items.forEach { item in
            retVal.append(DZConfigTypeElement(item))
        }
        
        return retVal
    }
}

extension DZConfigEventTypes : Decodable {
    enum CodingKeys: String, CodingKey {
        case typesV3 = "events_v3"
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)

        let typesV3 = try values.decodeIfPresent([DZConfigTypeElementV3].self, forKey: .typesV3)
        typesContent = typesV3 != nil ? DZConfigEventTypes.getTypesForTypeData(typesV3!, "content") : []
        typesAd = typesV3 != nil ? DZConfigEventTypes.getTypesForTypeData(typesV3!, "ad") : []
    }
}

