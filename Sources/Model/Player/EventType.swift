//
//  EventType.swift
//  DZCollectorTvOSBase
//
//  Created by Vuk on 24.11.21..
//  Copyright © 2021 RedCellApps. All rights reserved.
//

import Foundation

public enum EventType {
    case none
    
    case networkTypeChange
    case networkTimed
    
    case datazoomLoaded

    case error
    
    case milestone
    case heartbeat
    
    case volumeChange
    case mute
    case unmute

    case mediaRequest
    case bufferStart
    case bufferEnd
    case stallStart
    case stallEnd
    case mediaLoaded
    
    case contentLoaded
    
    case seekStart
    case seekEnd
    case play
    case playing
    case pause
    case resume
    
    case resize
    case fullscreen
    case exitFullscreen

    case playbackReady
    case playbackStart
    case playbackComplete
    
    case renditionChange
    case audioTrackChanged
    case qualityChangeRequest
    case subtitleChange
    
    case castStart
    case castEnd
    case playBtn
    
    case adSkipped
    case adBrakeStart
    case adBreakEnd
    case adImpression
    case adClick
    
    case custom(_ name: String, _ metadata:[String:Any]?)
    
    static let defaultType: Self = .none
    
    public var key: String {
        switch self {
        case .none:
            return "none"
            
        case .networkTypeChange:
            return "network_type_change"
        case .networkTimed:
            return "network_timed"
            
        case .datazoomLoaded:
            return "datazoom_loaded"

        case .error:
            return "error"
            
        case .milestone:
            return "milestone"
        case .heartbeat:
            return "heartbeat"
            
        case .volumeChange:
            return "volume_change"
        case .mute:
            return "mute"
        case .unmute:
            return "unmute"

        case .play:
            return "play"
        case .playing:
            return "playing"
        case .pause:
            return "pause"
        case .resume:
            return "resume"

        case .seekStart:
            return "seek_start"
        case .seekEnd:
            return "seek_end"
            
        case .mediaRequest:
            return "media_request"
        case .mediaLoaded:
            return "media_loaded"
            
        case .stallStart:
            return "stall_start"
        case .stallEnd:
            return "stall_end"
        case .bufferStart:
            return "buffer_start"
        case .bufferEnd:
            return "buffer_end"

            
        case .contentLoaded:
            return "content_loaded"
            
        case .resize:
            return "resize"
        case .fullscreen:
            return "fullscreen"
        case .exitFullscreen:
            return "exit_fullscreen"

        case .playbackReady:
            return "playback_ready"
        case .playbackStart:
            return "playback_start"
        case .playbackComplete:
            return "playback_complete"

        case .renditionChange:
            return "rendition_change"
        case .audioTrackChanged:
            return "audio_track_changed"
        case .qualityChangeRequest:
            return "quality_change_request"
        case .subtitleChange:
            return "subtitle_change"

        case .castStart:
            return "cast_start"
        case .castEnd:
            return "cast_end"
        case .playBtn:
            return "play_btn"
            
            
        case .adSkipped:
            return "ad_skip"
            
        case .adBrakeStart:
            return "ad_brake_start"
        case .adBreakEnd:
            return "ad_break_end"
            
        case .adImpression:
            return "ad_impression"
        case .adClick:
            return "ad_click"
            

        case .custom(let name, _):
            return "Custom_" + name
        }
    }
}

extension EventType : Hashable {
    public func hash(into hasher: inout Hasher) {
        hasher.combine(self.key)
    }
}

extension EventType : Identifiable {
    public var id: Self { self }
}

extension EventType : Equatable {
    public static func == (lhs: Self, rhs: Self) -> Bool {
        switch(lhs, rhs){
        case (.custom(_,_), .custom(_,_)):
            return true
            
        default:
            return lhs.hashValue == rhs.hashValue
        }
    }
}
