//
//  DZEventError.swift
//  DZCollectorTvOSBase
//
//  Created by Vuk on 7.12.21..
//  Copyright © 2021 Adhoc Technologies. All rights reserved.
//

import Foundation

public class DZEventError {
    public var code = String()
    public var message = String()
}

extension DZEventError : Encodable {
    enum CodingKeys: String, CodingKey, CaseIterable {
        case code = "error_code"
        case message = "error_msg"
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(code, forKey: .code)
        try container.encode(message, forKey: .message)
    }
}
