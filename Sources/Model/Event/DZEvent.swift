//
//  DZEvent.swift
//  DZCollectorTvOSBase
//
//  Created by Vuk on 1.12.21..
//  Copyright © 2021 RedCellApps. All rights reserved.
//

import Foundation

public class DZEvent {
    public var type: EventType = EventType.none {
        didSet {
            self.details.type = self.type
        }
    }
    
    public var eventId: String = String()
    public var configurationId: String = String()
    public var customerCode: String = String()
    public var connectorList: String = String()
    public var oAuthToken: String = String()
    
    public var userDetails: DZEventUserDetails = DZEventUserDetails()
    public var device: DZEventDevice = DZEventDevice()
    public var geoLocation: DZEventGeoLocation = DZEventGeoLocation()
    public var network: DZEventNetworkDetails = DZEventNetworkDetails()
    public var adData: DZEventAd = DZEventAd()
    public var page: DZEventPage = DZEventPage()
    public var sampling: DZEventSampling = DZEventSampling()
    public var opsMetadata: DZEventOpsMetadata = DZEventOpsMetadata()
    
    public var cmcd: DZEventCMCD = DZEventCMCD()
    public var cdn: DZEventCDN = DZEventCDN()
    
    public var player: DZEventPlayer = DZEventPlayer()
    public var video: DZEventVideo = DZEventVideo()
    
    public var details: DZEventDetails = DZEventDetails()

    public var customData: [String: Any]? = nil
    
//    public var sessionViewID                = String()
//    public var dzSdkVersion                = String()
//
//    public var toMilliss = Float64()
//    public var fromMilliss = Float64()
    
    public init() {
        type = EventType.none
        
        eventId = String()
        configurationId = String()
        customerCode = String()
        connectorList = String()
        oAuthToken = String()
        
        userDetails = DZEventUserDetails()
        device = DZEventDevice()
        geoLocation = DZEventGeoLocation()
        network = DZEventNetworkDetails()
        adData = DZEventAd()
        page = DZEventPage()
        sampling = DZEventSampling()
        opsMetadata = DZEventOpsMetadata()
        
        cmcd = DZEventCMCD()
        cdn = DZEventCDN()
        
        player = DZEventPlayer()
        video = DZEventVideo()
        
        details = DZEventDetails()
        customData = nil
    }
    
    public convenience init(_ type: EventType){
        self.init()
        self.type = type
        self.details.type = type
    }
}

extension DZEvent : Encodable {
    enum CodingKeys: String, CodingKey {
        case eventId = "event_id"
        case configurationId = "configuration_id"
        case customerCode = "customer_code"
        case connectorList = "connector_list"
        
        //case header = "header"
        case userDetails = "user_details"
        case device = "device"
        case geoLocation = "geo_location"
        case player = "player"
        case cmcd = "cmcd"
        case cdn = "cdn"
        case video = "video"
        case network = "network"
        case adData = "ad"
        case page = "page"
        case sampling = "sampling"
        
        case event = "event"
        case customData = "custom"
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        //try container.encode(header, forKey: .header)
        try container.encode(eventId, forKey: .eventId)
        try container.encode(configurationId, forKey: .configurationId)
        try container.encode(customerCode, forKey: .customerCode)
        try container.encode(connectorList, forKey: .connectorList)
        
        try container.encode(userDetails, forKey: .userDetails)
        try container.encode(device, forKey: .device)
        try container.encode(geoLocation, forKey: .geoLocation)
        try container.encode(player, forKey: .player)
        try container.encode(cmcd, forKey: .cmcd)
        try container.encode(cdn, forKey: .cdn)
        try container.encode(video, forKey: .video)
        try container.encode(network, forKey: .network)
        if(video.mediaType == .ad){
            try container.encode(adData, forKey: .adData)
        }
        try container.encode(page, forKey: .page)
        try container.encode(sampling, forKey: .sampling)

        print("Event - Before encoding \(type.key) \(details.attributes.configuration)")
        try container.encode(details, forKey: .event)

        try container.encodeIfPresent(customData, forKey: .customData)
    }
}

extension DZEvent {
    public func initWithConfiguration(meta: [String], flux: [String], mediaType: VideoType) {
        self.eventId = UUID().uuidString.lowercased()
        
        let configurationId = DZWebBroker.shared.getConfigurationId()
        self.configurationId = (configurationId.isEmpty) ? "NA" : configurationId
        
        let customerCode = DZWebBroker.shared.getCustomerCode()
        self.customerCode = (customerCode.isEmpty) ? "NA" : customerCode
        
        let connectorList = DZWebBroker.shared.getConnectorList()
        self.connectorList = (connectorList.isEmpty) ? "NA" : connectorList
        
        let token = DZWebBroker.shared.getOAuthToken()
        self.oAuthToken = (token.isEmpty) ? "NA" : token
        
        userDetails.initWithConfiguration(meta)
        device.initWithConfiguration(meta)
        geoLocation.initWithConfiguration(meta)
        player.initWithConfiguration(meta)
        cmcd.initWithConfiguration(meta)
        video.initWithConfiguration(meta)
        network.initWithConfiguration(meta)
        adData.initWithConfiguration(meta)
        page.initWithConfiguration(meta)
        //sampling.initWithConfiguration(meta)
        
        details.initWithConfiguration(type: self.type, mediaType: mediaType, meta: meta, flux: flux)
        print("Event - After init with configuration \(type.key) \(details.attributes.configuration)")
    }
}

