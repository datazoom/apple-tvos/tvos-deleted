//
//  DZEventInfoHeader.swift
//  DZCollectorTvOSBase
//
//  Created by Vuk on 1.12.21..
//  Copyright © 2021 RedCellApps. All rights reserved.
//

import Foundation

class DZEventHeader {
    public var eventId: String = String()
    public var configurationId : String = String()
    public var customerCode: String = String()
    public var connectorList : String = String()
    
    public init() {
        self.eventId = UUID().uuidString.lowercased()
        self.configurationId = String()
        self.customerCode = String()
        self.connectorList = String()
    }
    
    public func initWithConfiguration(){        
        let customerCode = DZWebBroker.shared.getCustomerCode()
        self.customerCode = (customerCode.isEmpty) ? "NA" : customerCode
        
        let connectorList = DZWebBroker.shared.getConnectorList()
        self.connectorList = (connectorList.isEmpty) ? "NA" : connectorList
        
        let configurationId = DZWebBroker.shared.getConfigurationId()
        self.connectorList = (configurationId.isEmpty) ? "NA" : connectorList
    }
}

extension DZEventHeader : Encodable {
    enum CodingKeys: String, CodingKey {
        case eventId = "event_id"
        case configurationId = "configuration_id"
        case customerCode = "customer_code"
        case connectorList = "connector_list"
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(eventId, forKey: .eventId)
        try container.encode(configurationId, forKey: .configurationId)
        try container.encode(customerCode, forKey: .customerCode)
        try container.encode(connectorList, forKey: .connectorList)
    }
}
