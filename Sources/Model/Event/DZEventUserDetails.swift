//
//  DZEventUserDetails.swift
//  DZCollectorTvOSBase
//
//  Created by Vuk on 1.12.21..
//  Copyright © 2021 RedCellApps. All rights reserved.
//

import Foundation

public class DZEventUserDetails {
    public var appSessionId: String = String()
    public var contentSessionId: String = String()
    public var clientIp: String = String()
    public var userAgent: String = String()
    public var isAnonymous: Bool = Bool()
    public var appSessionStart: Double = Double()
    public var oAuthToken: String = String()
    
    var configuration: [CodingKeys] = []
    
    public init() {
        self.appSessionId = ""
        self.contentSessionId = ""
        self.clientIp = DZEventUserDetails.getIPAddress() ?? ""
        self.userAgent = ""
        self.isAnonymous = false
        self.appSessionStart = Double(0)
        self.oAuthToken = ""
    }
}

extension DZEventUserDetails {
    //MARK:- Get IPAddress
    private class func getIPAddress() -> String? {
        var address: String?
        var ifaddr: UnsafeMutablePointer<ifaddrs>? = nil
        
        if getifaddrs(&ifaddr) == 0 {
            var ptr = ifaddr
            while ptr != nil {
                defer { ptr = ptr?.pointee.ifa_next }
                
                let interface = ptr?.pointee
                let ifa_name = interface?.ifa_name
                let name: String = String(cString: ifa_name!)
                let addrFamily = interface?.ifa_addr.pointee.sa_family
                if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                    if name == "en0" {
                        var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                        getnameinfo(interface?.ifa_addr, socklen_t((interface?.ifa_addr.pointee.sa_len)!), &hostname, socklen_t(hostname.count), nil, socklen_t(0), NI_NUMERICHOST)
                        address = String(cString: hostname)
                    }
                }
            }
            freeifaddrs(ifaddr)
        }
        return address
    }

    private class func getIPAddressV2() -> String? {
        var address: String?
        var ifaddr: UnsafeMutablePointer<ifaddrs>?
        
        if getifaddrs(&ifaddr) == 0 {
            var ptr = ifaddr
            while ptr != nil {
                defer { ptr = ptr?.pointee.ifa_next }
                
                guard let interface = ptr?.pointee else {
                    return nil
                }
                let addrFamily = interface.ifa_addr.pointee.sa_family
                if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                    
                    guard let ifa_name = interface.ifa_name else {
                        return nil
                    }
                    let name: String = String(cString: ifa_name)
                    
                    if name == "en0" {  // String.fromCString() is deprecated in Swift 3. So use the following code inorder to get the exact IP Address.
                        var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                        getnameinfo(interface.ifa_addr, socklen_t((interface.ifa_addr.pointee.sa_len)), &hostname, socklen_t(hostname.count), nil, socklen_t(0), NI_NUMERICHOST)
                        address = String(cString: hostname)
                    }
                }
            }
            freeifaddrs(ifaddr)
        }
        return address
    }


    //MARK:- Get MAC IP Address
    private class func getIFAddresses() -> String? {
        var addresses = [String()]
        
        var ifaddr : UnsafeMutablePointer<ifaddrs>?
        guard getifaddrs(&ifaddr) == 0 else { return "" }
        guard let firstAddr = ifaddr else { return "" }
        
        for ptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
            let flags = Int32(ptr.pointee.ifa_flags)
            let addr = ptr.pointee.ifa_addr.pointee
            
            if (flags & (IFF_UP|IFF_RUNNING|IFF_LOOPBACK)) == (IFF_UP|IFF_RUNNING) {
                if addr.sa_family == UInt8(AF_INET) || addr.sa_family == UInt8(AF_INET6) {
                    
                    var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                    if (getnameinfo(ptr.pointee.ifa_addr, socklen_t(addr.sa_len), &hostname, socklen_t(hostname.count),
                                    nil, socklen_t(0), NI_NUMERICHOST) == 0) {
                        let address = String(cString: hostname)
                        addresses.append(address)
                    }
                }
            }
        }
        
        freeifaddrs(ifaddr)
        return addresses.count > 1  ? addresses[2] : "192.168.31.253"
    }
}

extension DZEventUserDetails : Encodable {
    enum CodingKeys: String, CodingKey, CaseIterable {
        case appSessionId = "app_session_id"
        case contentSessionId = "content_session_id"
        case clientIp = "client_ip"
        case userAgent = "user_agent"
        case isAnonymous = "is_anonymous"
        case appSessionStart = "app_session_start_ts_ms"
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        if configuration.contains(.appSessionId) {
            try container.encode(appSessionId, forKey: .appSessionId)
        }
        if configuration.contains(.contentSessionId) {
            try container.encode(contentSessionId, forKey: .contentSessionId)
        }
        if configuration.contains(.clientIp) {
            try container.encode(clientIp, forKey: .clientIp)
        }
        if configuration.contains(.userAgent) {
            try container.encode(userAgent, forKey: .userAgent)
        }
        if configuration.contains(.isAnonymous) {
            try container.encode(isAnonymous, forKey: .isAnonymous)
        }
        if configuration.contains(.userAgent) {
            try container.encode(userAgent, forKey: .userAgent)
        }
        if configuration.contains(.appSessionStart) {
            try container.encode(appSessionStart, forKey: .appSessionStart)
        }
    }
    
    public func initWithConfiguration(_ from: [String]){
        configuration = []
        CodingKeys.allCases.forEach { item in
            if from.contains(item.stringValue) {
                configuration.append(item)
            }
        }
    }
}
