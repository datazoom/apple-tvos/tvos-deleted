//
//  DZEventPlayer.swift
//  DZCollectorTvOSBase
//
//  Created by Vuk on 1.12.21..
//  Copyright © 2021 RedCellApps. All rights reserved.
//

import Foundation

public class DZEventPlayer {
    public var playerVersion: String = String()
    public var playerName: String = String()
    public var readyState: String = String()
    public var defaultPlaybackRate: Float = Float()
    public var streamingProtocol: String = String()
    public var streamingType: String = String()
    public var autostart: Bool = Bool()
    public var loop: Bool = Bool()
    public var controls: Bool = Bool()
    public var fullscreen: Bool = Bool()
        
    var configuration: [CodingKeys] = []
}

extension DZEventPlayer : Encodable {
    enum CodingKeys: String, CodingKey, CaseIterable {
        case playerVersion = "player_version"
        case playerName = "player_name"
        case readyState = "ready_state"
        case defaultPlaybackRate = "default_playback_rate"
        case streamingProtocol = "streaming_protocol"
        case streamingType = "streaming_type"
        case autostart = "autostart"
        case loop = "loop"
        case controls = "controls"
        case fullscreen = "fullscreen"
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(playerVersion, forKey: .playerVersion)
        if configuration.contains(.playerName) {
            try container.encode(playerName, forKey: .playerName)
        }
        if configuration.contains(.readyState) {
            try container.encode(readyState, forKey: .readyState)
        }
        if configuration.contains(.defaultPlaybackRate) {
            try container.encode(defaultPlaybackRate, forKey: .defaultPlaybackRate)
        }
        if configuration.contains(.streamingProtocol) {
            try container.encode(streamingProtocol, forKey: .streamingProtocol)
        }
        if configuration.contains(.streamingType) {
            try container.encode(streamingType, forKey: .streamingType)
        }
        if configuration.contains(.autostart) {
            try container.encode(autostart, forKey: .autostart)
        }
        if configuration.contains(.loop) {
            try container.encode(loop, forKey: .loop)
        }
        if configuration.contains(.controls) {
            try container.encode(controls, forKey: .controls)
        }
        if configuration.contains(.fullscreen) {
            try container.encode(fullscreen, forKey: .fullscreen)
        }
    }
    
    public func initWithConfiguration(_ from: [String]){
        configuration = []
        CodingKeys.allCases.forEach { item in
            if from.contains(item.stringValue) {
                configuration.append(item)
            }
        }
    }
}
