//
//  DZEventOpsMetadata.swift
//  DZCollectorTvOSBase
//
//  Created by Vuk on 8.12.21..
//  Copyright © 2021 Adhoc Technologies. All rights reserved.
//

import Foundation

public class DZEventOpsMetadata {
    var serverTimestampOffset: Int = Int()
}

extension DZEventOpsMetadata : Encodable {
    enum CodingKeys: String, CodingKey {
        case serverTimestampOffset = "server_ts_offset_ms"
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(serverTimestampOffset, forKey: .serverTimestampOffset)
    }
}
