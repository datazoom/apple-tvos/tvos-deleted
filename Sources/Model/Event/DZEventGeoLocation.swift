//
//  DZEventGeoLocation.swift
//  DZCollectorTvOSBase
//
//  Created by Vuk on 1.12.21..
//  Copyright © 2021 RedCellApps. All rights reserved.
//

import Foundation

public class DZEventGeoLocation {
    public var city : String
    public var country : String
    public var countryCode : String
    public var latitude : Double
    public var longitude : Double
    public var regionName : String
    public var regionCode : String
    public let postalCode : String?
    
    var configuration: [CodingKeys] = []
    
    public init() {
        self.city = ""
        self.country = ""
        self.countryCode = ""
        self.latitude = 0
        self.longitude  = 0
        self.regionCode = ""
        self.regionName = ""
        self.postalCode = ""
    }
    
    public func initFromConfiguration(_ net: DZConfigNetworkDetails?) {
        if let geo = net {
            self.city = geo.city ?? ""
            self.country = geo.country ?? ""
            self.countryCode = geo.countryCode ?? ""
            self.latitude = geo.latitude ?? 0
            self.longitude = geo.longitude ?? 0
            self.regionCode = geo.region ?? ""
            self.regionName = geo.regionName ?? ""
//        self.postalCode = ""
        }
    }
}

extension DZEventGeoLocation : Encodable {
    enum CodingKeys: String, CodingKey, CaseIterable {
        case city = "city"
        case country = "country"
        case countryCode = "country_code"
        case latitude = "latitude"
        case longitude = "longitude"
        case regionCode = "region_code"
        case regionName = "region"
        case postalCode = "postal_code"
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        if configuration.contains(.city) {
            try container.encode(city, forKey: .city)
        }
        if configuration.contains(.country) {
            try container.encode(country, forKey: .country)
        }
        if configuration.contains(.countryCode) {
            try container.encode(countryCode, forKey: .countryCode)
        }
        if configuration.contains(.latitude) {
            try container.encode(latitude, forKey: .latitude)
        }
        if configuration.contains(.longitude) {
            try container.encode(longitude, forKey: .longitude)
        }
        if configuration.contains(.regionName) {
            try container.encode(regionName, forKey: .regionName)
        }
        if configuration.contains(.regionCode) {
            try container.encode(regionCode, forKey: .regionCode)
        }
        if configuration.contains(.postalCode) {
            try container.encode(postalCode, forKey: .postalCode)
        }
    }
    
    public func initWithConfiguration(_ from: [String]){
        configuration = []
        CodingKeys.allCases.forEach { item in
            if from.contains(item.stringValue) {
                configuration.append(item)
            }
        }
    }
}
