//
//  DZEventDetailsAttributes.swift
//  DZCollectorTvOSBase
//
//  Created by Vuk on 8.2.22..
//  Copyright © 2022 Adhoc Technologies. All rights reserved.
//

import Foundation

public class DZEventDetailsAttributes {
    public var error: DZEventError = DZEventError()
    public var milestonePercent: Double = Double()
    public var seekStart: Int = 0
    public var seekEnd: Int = 0
    public var absShift: String = ""
    public var startupDurationContent: Int = 0
    public var startupDurationTotal: Int = 0
    
    var configuration: [AttributesCodingKeys] = []
}

extension DZEventDetailsAttributes : Encodable {
    enum AttributesCodingKeys: String, CodingKey, CaseIterable {
        case errorCode = "error_code"
        case errorMessage = "error_msg"
        case milestonePercent = "milestone_percent"
        case seekStart = "seek_start_point_ms"
        case seekEnd = "seek_end_point_ms"
        case absShift = "abs_shift"
        case startupDurationContent = "startup_duration_content_ms"
        case startupDurationTotal = "startup_duration_total_ms"
    }
    
    public func encode(to encoder: Encoder) throws {
        print("Encoding \(configuration)")
        var container = encoder.container(keyedBy: AttributesCodingKeys.self)
        if configuration.contains(.errorCode) {
            try container.encode(error.code, forKey: AttributesCodingKeys.errorCode)
        }
        if configuration.contains(.errorMessage) {
            try container.encode(error.message, forKey: AttributesCodingKeys.errorMessage)
        }
        
        if configuration.contains(.milestonePercent) {
            try container.encode(milestonePercent, forKey: AttributesCodingKeys.milestonePercent)
        }
        
        if configuration.contains(.seekStart) {
            try container.encode(seekStart, forKey: AttributesCodingKeys.seekStart)
        }
        if configuration.contains(.seekEnd) {
            try container.encode(seekEnd, forKey: AttributesCodingKeys.seekEnd)
        }
        
        if configuration.contains(.absShift) {
            try container.encode(absShift, forKey: AttributesCodingKeys.absShift)
        }
        
        if configuration.contains(.startupDurationContent) {
            try container.encode(startupDurationContent, forKey: AttributesCodingKeys.startupDurationContent)
        }
        if configuration.contains(.startupDurationTotal) {
            try container.encode(startupDurationTotal, forKey: AttributesCodingKeys.startupDurationTotal)
        }
    }
    
    public func initWithConfiguration(type: EventType, mediaType: VideoType, from: [String]){
        let typesForOptionalAttributes: [EventType] = [.error, .milestone, .renditionChange, .seekEnd, .playbackStart]
        let optionalAttributes: [AttributesCodingKeys] = [.errorMessage, .errorCode, .milestonePercent, .absShift, .seekStart, .seekEnd, .startupDurationContent, .startupDurationTotal]
        
        configuration = []
        AttributesCodingKeys.allCases.forEach { item in
            if !typesForOptionalAttributes.contains(type) {
                if from.contains(item.stringValue) {
                    if !optionalAttributes.contains(item) {
                        configuration.append(item)
                        print("Configuration \(configuration)")
                    }
                }
            }
        }
        
        switch type {
        case .error:
            if from.contains(AttributesCodingKeys.errorMessage.stringValue) {
                if !configuration.contains(AttributesCodingKeys.errorMessage) {
                    configuration.append(AttributesCodingKeys.errorMessage)
                }
            }
            if from.contains(AttributesCodingKeys.errorCode.stringValue) {
                if !configuration.contains(AttributesCodingKeys.errorCode) {
                    configuration.append(AttributesCodingKeys.errorCode)
                }
            }
                
        case .milestone:
            if from.contains(AttributesCodingKeys.milestonePercent.stringValue) {
                if !configuration.contains(AttributesCodingKeys.milestonePercent) {
                    configuration.append(AttributesCodingKeys.milestonePercent)
                }
            }
            
        case .renditionChange:
            if from.contains(AttributesCodingKeys.absShift.stringValue) {
                if !configuration.contains(AttributesCodingKeys.absShift) {
                    configuration.append(AttributesCodingKeys.absShift)
                }
            }
            
        case .seekEnd:
            if from.contains(AttributesCodingKeys.seekStart.stringValue) {
                if !configuration.contains(AttributesCodingKeys.seekStart) {
                    configuration.append(AttributesCodingKeys.seekStart)
                }
            }
            if from.contains(AttributesCodingKeys.seekEnd.stringValue) {
                if !configuration.contains(AttributesCodingKeys.seekEnd) {
                    configuration.append(AttributesCodingKeys.seekEnd)
                }
            }
            
        case .playbackStart:
            if mediaType == .content {
                if from.contains(AttributesCodingKeys.startupDurationContent.stringValue) {
                    if !configuration.contains(AttributesCodingKeys.startupDurationContent) {
                        configuration.append(AttributesCodingKeys.startupDurationContent)
                    }
                }
            }
//            if mediaType == .ad {
//                if from.contains(AttributesCodingKeys.startupDurationAd.stringValue) && !configuration.contains(AttributesCodingKeys.startupDurationAd) {
//                    configuration.append(AttributesCodingKeys.startupDurationAd)
//                }
//            }
            if from.contains(AttributesCodingKeys.startupDurationTotal.stringValue) {
                if !configuration.contains(AttributesCodingKeys.startupDurationTotal) {
                    configuration.append(AttributesCodingKeys.startupDurationTotal)
                }
            }
            
        default:
            print("Event has no additonal attributes")
        }
        
        print ("############## Event type \(type) -  Event attribute list ### \(configuration)")
    }
    
}
