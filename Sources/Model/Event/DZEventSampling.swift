//
//  DZEventSampling.swift
//  DZCollectorTvOSBase
//
//  Created by Vuk on 1.12.21..
//  Copyright © 2021 RedCellApps. All rights reserved.
//

import Foundation

public class DZEventSampling {
    public var samplingRate: Double = 0
    public var inSample: Bool = false
}

extension DZEventSampling : Encodable {
    enum CodingKeys: String, CodingKey {
        case samplingRate = "connector_sampling_rate"
        case inSample = "in_sample"
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(samplingRate, forKey: .samplingRate)
        try container.encode(inSample, forKey: .inSample)
    }
}
