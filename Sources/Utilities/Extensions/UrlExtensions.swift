//
//  UrlExtension.swift
//  DZ_Adapter_tvOS_Native
//
//  Created by Vuk on 5.11.21..
//  Copyright © 2021 RedCellApps. All rights reserved.
//

import Foundation
import UniformTypeIdentifiers
#if !os(macOS)
import MobileCoreServices
#endif

public extension URL {
    func mimeType() -> String {
        let pathExtension = self.pathExtension.lowercased()

        if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension as NSString, nil)?.takeRetainedValue() {
            if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
                return mimetype as String
            }
        }
        return "application/octet-stream"
    }
    
    func streamingProtocol() -> String {
        let mimeToStreamingProtocol : [String: String] =
        ["application/x-mpegURL" : "HLS",
         "application/dash+xml" : "DASH",
         "application/octet-stream" : "Smooth",
         "video/x-flv" : "Flash",
         "video/mp4" : "MP4",
         "video/MP2T" : "MPEG2-TS",
         "video/3gpp" : "3GPP",
         "video/quicktime" : "Quicktime",
         "video/x-msvideo" : "AVI",
         "video/x-ms-wmv" : "Windows Media"
        ]
        
        let extensionToStreamingProtocol : [String: String] =
        ["m3u" : "HLS",
         "m3u8" : "HLS",
         "flv" : "Flash",
         "mp4" : "MP4",
         "m4a" : "MP4",
         "m4p" : "MP4",
         "m4b" : "MP4",
         "m4r" : "MP4",
         "m4v" : "MP4",
         "ts" : "MPEG2-TS",
         "3gp" : "3GPP",
         "mov" : "Quicktime",
         "avi" : "AVI",
         "wmv" : "Windows Media"
        ]

        let pathExtension = self.pathExtension.lowercased()
        let sprotocol = (extensionToStreamingProtocol[pathExtension] != nil) ? extensionToStreamingProtocol[pathExtension] : "Other"
        return sprotocol!
    }
    
    func streamingType() -> String {
        //let streamingType : [String] = ["Live", "VOD"]
        let pathExtension = self.pathExtension.lowercased()
        var stype = ""
        
        switch(pathExtension.lowercased()){
        case "m3u", "m3u8":
            stype = "Live"
            break

        case "flv","mp4","m4a","m4p","m4b","m4r","m4v","ts","3gp","mov","avi","wmv":
            stype = "VOD"
        
        default:
            break
        }
        return stype
    }
    
    var containsImage: Bool {
        let mimeType = self.mimeType()
        guard let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassMIMEType, mimeType as CFString, nil)?.takeRetainedValue() else {
            return false
        }
        return UTTypeConformsTo(uti, kUTTypeImage)
    }
    var containsAudio: Bool {
        let mimeType = self.mimeType()
        guard let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassMIMEType, mimeType as CFString, nil)?.takeRetainedValue() else {
            return false
        }
        return UTTypeConformsTo(uti, kUTTypeAudio)
    }
    var containsVideo: Bool {
        let mimeType = self.mimeType()
        guard  let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassMIMEType, mimeType as CFString, nil)?.takeRetainedValue() else {
            return false
        }
        return UTTypeConformsTo(uti, kUTTypeMovie)
    }

}

